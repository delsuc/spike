# README #

This repository is a backup place for the SPIKE data analysis program.

The main location for spike is here : https://github.com/spike-project

### Why are you here ? ###
The reason for this repository is that SPIKE was stored on bitbucket when it was originally started *circa* 2011.
At that time, we were using `hg` (mercurial) for the versionning system - and was happy with it.
We were also happy with the way bitbucket.org was dealing the different project.

Then bitbucket.org decided to completely drop support for `hg`, even for existing projects,
and to switch everything to `git`.
In the mean time, `github.com` had become a standard *de facto*, was bought by Microsft 
and changed considerably their policies.

Altogether, as we had no choice but to move away from `hg` we also moved away from bitbucket.og.

Site site is a remnant of the old times...



